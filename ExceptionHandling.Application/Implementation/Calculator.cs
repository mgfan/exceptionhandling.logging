﻿using System.Linq;
using Calculation.Interfaces;
using System.Text;

namespace Calculation.Implementation
{
	

	public class Calculator : BaseCalculator, ICalculator
    {
		readonly private ILogger log;
		public Calculator(ILogger logger)
		{
            this.log = logger;
        }
        public int Sum(params int[] numbers)
        {
			log.Trace($"Выполняется метод Sum , с параметрами {NumbersToString(numbers)}");
            try
            {
				var sum = SafeSum(numbers);
				log.Info($"Метод Sum успешно выполнен с параметрами - {NumbersToString(numbers)}, результат выполнение метода {sum}");
				return sum;
			}
            catch (System.OverflowException ex)
            {
				log.Error(ex);
				throw;
            }
            finally
            {
				log.Trace("Метод Sum завершил работу");
			}
		}

		public int Sub(int a, int b)
		{
            try
            {
				return SafeSub(a, b);
			}
			catch(System.OverflowException)
            {
				throw new System.OverflowException(nameof(Sub));
            }
		}

		public int Multiply(params int[] numbers)
		{
			try
            {
				if (!numbers.Any())
					return 0;

				return SafeMultiply(numbers);
			}
			catch(System.OverflowException ex)
            {
				throw new System.InvalidOperationException("Некорректные данные",ex);
            }
		}

		public int Div(int a, int b)
		{
            try
            {
				return a / b;
			}
			catch(System.DivideByZeroException)
            {
				throw new System.InvalidOperationException("Деление на ноль запрещено");
            }
		}


		private static string NumbersToString(params int[] numbers)
		{
			int schet = 0;
			StringBuilder numbersToSb = new StringBuilder(numbers.Length);
			foreach (int number in numbers)
			{
				if (schet == numbers.Length - 1)
				{
					numbersToSb.Append(number);
				}
				else
				{
					numbersToSb.Append(number + " , ");
					schet += 1;
				}
			}
			return numbersToSb.ToString();
		}
	}
}