﻿using NLog;
using System;

namespace Calculation
{
    public class Log : Interfaces.ILogger
	{
		private readonly NLog.Logger logger = LogManager.GetCurrentClassLogger();
		public void Trace(string message)
		{
			logger.Trace(message);
		}
		public void Info(string message)
		{
			logger.Info(message);
		}
		public void Error(Exception ex)
		{
            logger.Error(ex.ToString());
        }
	}
}
