﻿using System;

namespace Calculation.Interfaces
{
	public interface ILogger
	{
		void Error(Exception ex);
		void Info(string message);
		void Trace(string message);
	}
	
}